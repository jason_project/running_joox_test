/*
 * @Author: Jason
 * @Date: 2019-12-24 09:58:06
 * @LastEditors  : Jason
 * @LastEditTime : 2020-01-10 15:32:04
 */
class PreCownDown extends egret.DisplayObjectContainer {
    preCountTimeText1: egret.TextField;
    preCountTimeText2: egret.TextField;
    gameStart: boolean;
    public constructor () {
        super();
        this.gameStart = false;
        this.once(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);

    }
    private preCountTime = 3;

    private props = {
        headerHeight: 0,
        headerBarHeight: 0,
        safeX: 0,
        safeY: 0,
        safeWidth: 0,
        safeHeight: 0,
        ratioX: 0,
        ratio: 0,
    }

    public initStageProps () {
        let initStageProps = window['_initStageProps'];
        this.props = initStageProps();
    }

    private onAddToStage () {
        // this.stage.scaleMode = egret.StageScaleMode.NO_BORDER
        this.initStageProps();
        this.renderBg();
        this.renderPreCountTime();
        this.renderTapHandle();
        setTimeout(() => {
            this.preCountTimeText1.size = 1;
            this.preCountTimeText1.alpha = 1;
            this.preCountTimeText1.anchorOffsetX = this.preCountTimeText1.width / 2;
            this.countDownPreTime();
        }, 300)
    }

    private renderBg () {
        const { safeY, headerBarHeight } = this.props;
        const maskBg = new egret.Shape();
        maskBg.x = this.stage.stageWidth / 2;
        maskBg.y = safeY + headerBarHeight;
        // console.log(safeY, headerBarHeight, safeY + headerBarHeight)
        // maskBg.x = 0;
        // maskBg.y = 0;
        maskBg.width = this.stage.stageWidth
        maskBg.height = this.stage.stageHeight;
        maskBg.anchorOffsetX = maskBg.width / 2;
        maskBg.alpha = 0.8;
        maskBg.graphics.beginFill(0x000000);
        maskBg.graphics.drawRect(0, 0, maskBg.width, maskBg.height);
        maskBg.graphics.endFill();
        this.addChild(maskBg);
        const tw = egret.Tween.get(maskBg);
        tw.to({ "alpha": 0.8 }, 200);
    }
    

    private renderPreCountTime () {
        const { ratio, safeY, headerBarHeight } = this.props;

        const preCountTimeText1:egret.TextField = new egret.TextField();
        const preCountTimeText2:egret.TextField = new egret.TextField();
        preCountTimeText1.textColor = 0xffffff;
        preCountTimeText1.stroke = 3;
        preCountTimeText1.strokeColor = 0x000000;
        preCountTimeText1.size = 100 * ratio;
        preCountTimeText1.fontFamily = 'Joox Brand Regular';
        preCountTimeText1.text = this.preCountTime.toString();
        preCountTimeText1.alpha = 0;
        preCountTimeText1.scaleX = 0;
        preCountTimeText1.scaleY = 0;
        preCountTimeText1.x = this.stage.stageWidth / 2;
        preCountTimeText1.y = safeY + headerBarHeight + 120 * ratio;

        this.addChild(preCountTimeText1);
        preCountTimeText1.anchorOffsetX = preCountTimeText1.width / 2;
        preCountTimeText1.anchorOffsetY = preCountTimeText1.height / 2;
        this.preCountTimeText1 = preCountTimeText1;

        preCountTimeText2.textColor = 0xffffff;
        preCountTimeText2.stroke = 3;
        preCountTimeText2.strokeColor = 0x000000;
        preCountTimeText2.size = 100 * ratio;
        preCountTimeText2.fontFamily = 'Joox Brand Regular';
        preCountTimeText2.text = this.preCountTime.toString();
        preCountTimeText2.alpha = 0;
        preCountTimeText2.scaleX = 0;
        preCountTimeText2.scaleY = 0;
        preCountTimeText2.x = this.stage.stageWidth / 2;
        preCountTimeText2.y = safeY + headerBarHeight + 120 * ratio;
        this.addChild(preCountTimeText2);
        preCountTimeText2.anchorOffsetX = preCountTimeText2.width / 2;
        preCountTimeText2.anchorOffsetY = preCountTimeText2.height / 2;
        this.preCountTimeText2 = preCountTimeText2;
        if (window['_language'] === 'my') {
            // preCountTimeText1.y = preCountTimeText1.y - 20;
            // preCountTimeText2.y = preCountTimeText2.y - 20;
        }
    }

    private countDownPreTime () {
        try {
            if (this.gameStart) return false;
            const ratio = this.props.ratioX;
            let preCountTime = this.preCountTime;
            const preCountTimeText1 = this.preCountTimeText1;
            preCountTimeText1.size = 100 * ratio;
            this.dispatchEvent(new egret.Event("COUNTDOWN", false, false, true))
                    
            preCountTimeText1.alpha = 1;
            preCountTimeText1.scaleX = 1.4;
            preCountTimeText1.scaleY = 1.4;
            preCountTimeText1.text = preCountTime.toString()
            preCountTimeText1.anchorOffsetX = preCountTimeText1.width / 2;
            preCountTimeText1.anchorOffsetY = preCountTimeText1.height / 2;
            let tw1 = egret.Tween.get(preCountTimeText1);

            tw1.to({ "alpha": 1, "scaleX": 0.8, "scaleY": 0.8}, 500);
            // this.dispatchEvent(new egret.Event("COUNTDOWN_START", false, false, true))
            tw1.wait(200);
            tw1.to({ "alpha": 0}, 100);
            tw1.call(() => {
                preCountTime--;
                const preCountTimeText2 = this.preCountTimeText2;
                let tw3 = egret.Tween.get(preCountTimeText2);
                preCountTimeText2.text = preCountTime.toString();
                preCountTimeText2.anchorOffsetX = preCountTimeText2.width / 2;
                preCountTimeText2.anchorOffsetY = preCountTimeText2.height / 2;
                this.dispatchEvent(new egret.Event("COUNTDOWN", false, false, true))
                preCountTimeText2.alpha = 1;
                preCountTimeText2.scaleX = 1.4;
                preCountTimeText2.scaleY = 1.4;
                tw3.to({ "alpha": 1, "scaleX": 0.8, "scaleY": 0.8}, 400);
                tw3.wait(100);
                tw3.to({ "alpha": 0}, 100);
                tw3.call(() => {
                    preCountTime--;
                    preCountTimeText1.text = preCountTime.toString();
                    preCountTimeText1.anchorOffsetX = preCountTimeText1.width / 2;
                    preCountTimeText1.anchorOffsetY = preCountTimeText1.height / 2;
                    let tw4 = egret.Tween.get(preCountTimeText1);
                    this.dispatchEvent(new egret.Event("COUNTDOWN", false, false, true))
                    preCountTimeText1.alpha = 1;
                    preCountTimeText1.scaleX = 1.4;
                    preCountTimeText1.scaleY = 1.4;
                    tw4.to({ "alpha": 1, "scaleX": 0.8, "scaleY": 0.8}, 400);
                    tw4.wait(100);
                    tw4.to({ "alpha": 0}, 100);
                    tw4.call(() => {
                        preCountTime--;
                        preCountTimeText2.text =  window['myTexts']['go'];
                        preCountTimeText2.size = (window['myTexts']['goSize'] || 100) * ratio;
                        preCountTimeText2.anchorOffsetX = preCountTimeText2.width / 2;
                        preCountTimeText2.anchorOffsetY = preCountTimeText2.height / 2;
                        preCountTimeText2.alpha = 1;
                        preCountTimeText2.scaleX = 1.3;
                        preCountTimeText2.scaleY = 1.3;
                        let tw5 = egret.Tween.get(preCountTimeText2);
                        this.dispatchEvent(new egret.Event("COUNTDOWN_END", false, false, true))
                        tw5.to({ "alpha": 1, "scaleX": 0.8, "scaleY": 0.8}, 400);
                        tw5.wait(100);
                        tw5.to({ "alpha": 0}, 100);
                        tw5.call(() => {
                            this.gameStart = true;
                            window['vconsole'] && window['vconsole']('precountdown finish');
                            this.dispatchEvent(new egret.Event("FINISH", false, false, true));
                        }, this);
                    }, this);
                }, this);
            }, this);
        } catch (e) {
            window['vconsole'] && window['vconsole']('precountdown:');
            window['vconsole'] && window['vconsole'](e);
        }
    }
    private renderTapHandle () {
        const props = this.props;
        const { ratioX: ratio, safeY, safeHeight } = props
        
        const group = new eui.Group();
        group.x = this.$stage.stageWidth / 2
        group.y = safeY + safeHeight - 100 * ratio
        
        const radius = 180 / 2 * ratio / 2;
        let circle = new egret.Shape();
        circle.width = radius * 2;
        circle.height = radius * 2;
        circle.x = 0;
        circle.y = 1 * ratio;
        circle.graphics.beginFill(0x000000);
        circle.graphics.lineStyle(4, 0x000000);
        circle.graphics.drawCircle(0, 0, radius);
        circle.graphics.endFill();
        let circle2 = new egret.Shape();
        circle2.width = radius * 2;
        circle2.height = radius * 2;
        circle2.x = 0;
        circle2.y = 0;
        circle2.graphics.beginFill(0xfff72a);
        circle2.graphics.lineStyle(4, 0x000000);
        circle2.graphics.drawCircle(0, 0, radius);
        circle2.graphics.endFill();
        
        group.addChild(circle);
        group.addChild(circle2);
        
        const btnText:egret.TextField = new egret.TextField();
        btnText.textColor = 0x000000;
        btnText.size = (window['myTexts']['tapFS'] || 18) * ratio;
        btnText.bold = typeof window['myTexts']['tapBold'] == 'undefined' ? true : window['myTexts']['tapBold'];
        btnText.fontFamily = window['myTexts']['tapFF'] || 'PingFang SC, helvetica neue, Microsoft YaHei';
        btnText.text = window['myTexts']['tap'];
        btnText.x = 0;
        btnText.y = 0;
        btnText.anchorOffsetX = btnText.width / 2;
        btnText.anchorOffsetY = btnText.height / 2;
        group.addChild(btnText);
        
        const tipText:egret.TextField = new egret.TextField();
        tipText.textColor = 0xfff72a;
        tipText.stroke = 3;
        tipText.strokeColor = 0x000000;
        tipText.size = (window['myTexts']['tapTipsFS'] || 14) * ratio;
        tipText.bold = typeof window['myTexts']['tapTipsBold'] == 'undefined' ? true : window['myTexts']['tapTipsBold'];
        tipText.fontFamily = window['myTexts']['tapTipsFF'] || 'PingFang SC, helvetica neue, Microsoft YaHei';
        tipText.text = window['myTexts']['tapTips'];
        tipText.x = 0;
        tipText.y = -radius - tipText.height - 45;
        tipText.anchorOffsetX = tipText.width / 2;
        group.addChild(tipText);

        let ring = new egret.Shape();
        ring.x = 0;
        ring.y = 0;
        ring.alpha = 0.8;
        this.drawRingProBar(ring, Math.PI * 2, radius, 2)
        ring.anchorOffsetX = ring.width / 2 -1;
        ring.anchorOffsetY = ring.height / 2 -1;
        group.addChild(ring)
        this.bounce(ring, 0.8, 1.5);
        let ring2 = new egret.Shape();
        ring2.x = 0;
        ring2.y = 0;
        ring2.alpha = 0.5;
        setTimeout(() => {
            this.bounce(ring2, 0.8, 1.5);
        }, 260)
        this.drawRingProBar(ring2, Math.PI * 2, radius, 1)
        ring2.anchorOffsetX = ring2.width / 2 -1;
        ring2.anchorOffsetY = ring2.height / 2 -1;
        group.addChild(ring2)
        
        this.addChild(group);
    }
    
    private bounce (result, alpha, scale) {
        let go = () => {
            result.scaleX = 1;
            result.scaleY = 1;
            result.alpha = alpha;
            let tw = egret.Tween.get(result);
            tw.to({ "scaleX": scale, "scaleY": scale, "alpha": 0 }, 600);
            tw.wait(300);
            tw.call(go, this);
        }
        go();
    }
    public drawRingProBar(shape: egret.Shape,angle: number,radius: number,line?: number, color?:number): void {
        if(shape && angle > 0) {
            var coangle = Math.PI / 2 - angle;
            var bigGap: number = 1;
            var smallgap: number = 1;
            var bigRadius: number = radius + bigGap;
            var smallRadis: number = radius - smallgap;
            var beginAngle: number = -Math.PI / 2;
            var endAngle: number = angle - Math.PI / 2;
            var halfAngle:number = Math.PI /2;
            var color = color ? color : 0xfff72a;
            var line = line ? line : 0;


            shape.graphics.clear();
            shape.graphics.lineStyle(line,color);
            shape.graphics.beginFill(color,1);


            if(angle <= Math.PI){
                shape.graphics.moveTo(radius,smallgap);
                shape.graphics.lineTo(radius,-bigGap);
                shape.graphics.drawArc(radius,radius,bigRadius,beginAngle,endAngle,false);
                shape.graphics.lineTo(Math.cos(coangle) * smallRadis + radius,radius - Math.sin(coangle) * smallRadis);
                shape.graphics.drawArc(radius,radius,smallRadis,endAngle,beginAngle,true);   
            }else{
                shape.graphics.moveTo(radius,smallgap);
                shape.graphics.lineTo(radius,-bigGap);
                shape.graphics.drawArc(radius,radius,bigRadius,beginAngle,halfAngle,false);
                shape.graphics.lineTo(radius,radius*2);
                shape.graphics.drawArc(radius,radius,smallRadis,halfAngle,beginAngle,true);    
            
                shape.graphics.moveTo(radius,radius*2-smallgap);
                shape.graphics.lineTo(radius,radius * 2+bigGap);
                shape.graphics.drawArc(radius,radius,bigRadius,halfAngle,endAngle,false);
                shape.graphics.lineTo(Math.cos(coangle) * smallRadis + radius,radius - Math.sin(coangle) * smallRadis);
                shape.graphics.drawArc(radius,radius,smallRadis,endAngle,halfAngle,true); 
            }
            shape.graphics.endFill();
        }
    }

    private createBitmap (name:string, props?) {
        const bitmap = this.createBitmapByName(name);
        props.x && (bitmap.x = props.x);
        props.y && (bitmap.y = props.y);
        return bitmap;
    }

    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
}