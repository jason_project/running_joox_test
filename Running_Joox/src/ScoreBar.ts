/*
 * @Author: Jason
 * @Date: 2021-10-29 09:58:06
 * @LastEditors  : Jason
 * @LastEditTime : 2020-01-10 15:32:04
 */
let musicOn = false;
class ScoreBar extends egret.DisplayObjectContainer {
    
    public constructor (options?) {
        super();
        if (options) {
            this.musicOn = options.musicOn || false;
            musicOn = options.musicOn;
        }
        this.score = 0;
        this.once(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);

    }
    
    score: number;
    musicOn: boolean;
    private musicIcon: egret.Bitmap;
    private scoreBar: eui.Group;
    private scoreWrap: egret.Shape;
    private progress_bar: egret.Shape;
    private progress_line: egret.Shape;
    private progress_jojo: egret.Bitmap;
    private progress_v: egret.Bitmap;
    private scoreDesc: egret.TextField;
    private scoreText: egret.TextField;
    private fullScore = 215;
    private total = 25;

    private props = {
        headerHeight: 0,
        headerBarHeight: 0,
        safeX: 0,
        safeY: 0,
        safeWidth: 0,
        safeHeight: 0,
        ratioX: 0,
        ratio: 0,
        scale: 1,
    }

    public initStageProps () {
        let initStageProps = window['_initStageProps'];
        this.props = initStageProps();
    }

    private onAddToStage () {
        this.initStageProps();
        this.renderScoreBar();
        this.renderScore();
        this.renderMusicIcon();
    }
    private renderScoreBar () {
        const props = this.props;
        const { ratio, safeX, safeY, safeWidth, headerHeight, headerBarHeight } = props;
        const group = new eui.Group();
        group.x = safeX;
        group.y = safeY - 1 * ratio;
        this.scoreBar = group;
        let scoreBarBg = new egret.Shape();
        const barWidth = safeWidth;
        const barHeight = headerBarHeight + 1 * ratio;
        scoreBarBg.width = barWidth;
        scoreBarBg.height = barHeight;
        // console.log(bar.width, progress.width)
        scoreBarBg.graphics.beginFill(0xffb42d);
        scoreBarBg.graphics.lineStyle(2.5, 0x000000);
        scoreBarBg.graphics.drawRect(0, 0, barWidth, barHeight);
        scoreBarBg.graphics.endFill();
        scoreBarBg.x = 0;
        scoreBarBg.y = 0;
        group.addChild(scoreBarBg);
        let scoreWrap = new egret.Shape();
        const radius = 60;
        scoreWrap.width = 240 + radius * 2;
        scoreWrap.height = headerHeight + radius * 2;
        scoreWrap.x = group.x-radius*2;
        scoreWrap.y = group.y-radius*2 - 16;
        scoreWrap.graphics.beginFill(0x000000);
        scoreWrap.graphics.drawRoundRect(0, 0, scoreWrap.width, scoreWrap.height, radius, radius);
        scoreWrap.graphics.endFill();
        this.scoreWrap = scoreWrap;
        this.addChild(group);
        this.addChild(scoreWrap);
        this.renderScoreProgress();
    }

    private renderScore () {
        const scoreBar = this.scoreBar
        const text:egret.TextField = new egret.TextField();
        text.textColor = 0xffb42d;
        text.size = 36;
        text.fontFamily = window['myTexts']['scoreFF'] || 'Joox Brand Regular';
        text.text = window['myTexts']['score'] || 'SCORE';
        text.x = scoreBar.x + 120;
        text.y = scoreBar.y + 16;
        text.anchorOffsetX = text.width / 2;
        this.scoreDesc = text;
        this.addChild(text);
        const scoreText:egret.TextField = new egret.TextField();
        scoreText.textColor = 0xfff72a;
        scoreText.size = 90;
        scoreText.fontFamily = 'Joox Brand Bold';
        scoreText.text = this.score.toString();
        scoreText.x = scoreBar.x + 120;
        scoreText.y = scoreBar.y + 95;
        scoreText.anchorOffsetX = scoreText.width / 2;
        scoreText.anchorOffsetY = scoreText.height / 2;
        this.scoreText = scoreText;
        this.addChild(scoreText);
    }

    public updateScore (score:number, i:number) {
        const { ratioX: ratio } = this.props
        this.score = score;
        const scoreText = this.scoreText
        scoreText.text = this.score.toString();
        scoreText.anchorOffsetX = scoreText.width / 2;
        scoreText.anchorOffsetY = scoreText.height / 2;

        const progress_bar = this.progress_bar
        const progress_line = this.progress_line
        const progress_jojo = this.progress_jojo
        let width = progress_bar.width - 15 * ratio;
        let progress = i / this.total;
        if (progress > 1) {
            progress = 1
        }
        progress_line.scaleX = progress
        
        let tw1 = egret.Tween.get(progress_jojo);
        tw1.to({x: progress_line.x + progress * width - 15 * ratio}, 100);
        let tw = egret.Tween.get(progress_line);
        tw.to({width: progress * width}, 200);
    }
    private renderScoreProgress () {
        const props = this.props;
        const { ratioX: ratio, safeX, safeY } = props
        
        const scoreGroup = new eui.Group();
        scoreGroup.x = safeX + 153 * ratio
        scoreGroup.y = safeY + 26 * ratio

        let bar = new egret.Shape();
        const barWidth = 150 * ratio;
        const barHeight = 18  * ratio;
        bar.width = barWidth;
        bar.height = barHeight;
        // console.log(bar.width, progress.width)
        bar.graphics.beginFill(0xffffff);
        bar.graphics.lineStyle(3, 0x000000);
        bar.graphics.drawRoundRect(0, 0, barWidth, barHeight, barHeight, barHeight);
        bar.graphics.endFill();
        bar.x = 0;
        bar.y = -9 * ratio;
        this.progress_bar = bar;
        scoreGroup.addChild(bar);
        let bar2 = new egret.Shape();
        const bar2Height = 16.5 * ratio;
        bar2.width = barWidth;
        bar2.height = bar2Height;
        bar2.graphics.beginFill(0xffffff);
        bar2.graphics.drawRoundRect(0, 0, barWidth, bar2Height, barWidth, bar2Height);
        bar2.graphics.endFill();
        bar2.x = 0;
        bar2.y = -8 * ratio;
        scoreGroup.addChild(bar2);

        const redius = 24 * ratio / 2;
        let circle = new egret.Shape();
        circle.width = redius * 2;
        circle.height = redius * 2;
        circle.x = 0;
        circle.y = 0;
        circle.graphics.beginFill(0xffe74d);
        circle.graphics.lineStyle(3, 0x000000);
        circle.graphics.drawCircle(0, 0, redius);
        circle.graphics.endFill();
        
        scoreGroup.addChild(circle);

        const redius2 = 16 * ratio / 2;
        let circle2 = new egret.Shape();
        circle2.width = redius * 2;
        circle2.height = redius * 2;
        circle2.x = redius - redius2 + 4;
        circle2.y = 0;
        circle2.graphics.beginFill(0xffe74d);
        circle2.graphics.drawCircle(0, 0, redius2);
        circle2.graphics.endFill();
        
        scoreGroup.addChild(circle2);

        
        let progress = new egret.Shape();
        const progressWidth = barWidth - 15 * ratio;
        const progressHeight = 16.5 * ratio;
        progress.width = 0;
        progress.height = progressHeight;
        // console.log(bar.width, progress.width)
        progress.graphics.beginFill(0xffe74d);
        progress.graphics.drawRoundRect(0, 0, progressWidth, progressHeight, progressHeight,progressHeight);
        progress.graphics.endFill();
        progress.scaleX = 0;
        progress.x = 0;
        progress.y = -8 * ratio;
        this.progress_line = progress;
        scoreGroup.addChild(progress);

        let progress_v = this.createBitmap('play_top_vip_png', {
            x: bar.x + bar.width - 16 * ratio,
            y: progress.y - 8 * ratio
        });
        progress_v.x = progress_v.x + progress_v.width / 2
        progress_v.y = progress_v.y + progress_v.height / 2
        progress_v.anchorOffsetX = progress_v.width / 2
        progress_v.anchorOffsetY = progress_v.height / 2
        this.progress_v = progress_v;
        scoreGroup.addChild(progress_v);
        
        let progress_jojo = this.createBitmap('play_top_jojo_png', {
            x: progress.x - 15 * ratio,
            y: progress.y - 7 * ratio
        });
        this.progress_jojo = progress_jojo;
        scoreGroup.addChild(progress_jojo);
        this.addChild(scoreGroup);
    }
    public shakeProgressVip () {
        let go = () => {
            let tw = egret.Tween.get(this.progress_v );
            tw.to({ "rotation": -15 }, 200);
            tw.to({ "rotation": 0 }, 200);
            tw.to({ "rotation": -15 }, 200);
            tw.to({ "rotation": 0 }, 200);
            tw.wait(1000);
            tw.call(go, this);
        }
        go();
    }

    private renderMusicIcon () {
        const props = this.props
        const { ratioX: ratio  } = props
        let name = this.musicOn ? 'sound_on_png' : 'sound_off_png';
        const obj = this.createBitmapButton(name, {
            x: props.safeX + props.safeWidth - 26 * ratio,
            y: props.safeY + 26 * ratio
        })
        const musicIcon = obj.bitmap;
        this.musicIcon = musicIcon;
        if (this.musicOn) {
            this.rotateMusicIcon(obj.bitmap);
            if (musicOn) {
                // console.log(this.musicOn, musicOn)
                this.dispatchEvent(new egret.Event("STOP_MUSIC", false, false, true));
            }
        }
        obj.button.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.toggleMusicIcon(obj.bitmap);
        }, this)
    }

    
    private rotateMusicIcon (result: egret.Bitmap) {
        let go = () => {
            let tw = egret.Tween.get(result);
            tw.to({ "rotation": 360 }, 1500);
            result.rotation = 0;
            tw.call(go, this);
        }
        go();
    }
    private toggleMusicIcon (result: egret.Bitmap) {
        this.musicOn = !this.musicOn;
        const musicOn = this.musicOn;
        window['sessionStorage']['musicOn'] = musicOn ? '1' : '0';
        let name = musicOn ? 'sound_on_png' : 'sound_off_png';
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        if (musicOn) {
            this.rotateMusicIcon(result);
        } else {
            egret.Tween.removeTweens(result);
            result.rotation = 0;
        }
        console.log(this.musicOn)
        this.dispatchEvent(new egret.Event("TOGGLE_MUSIC", false, false, true))
    }
    private createBitmapButton (name: string, props) {
        let bitmap = this.createBitmapByName(name);
        this.addChild(bitmap);
        bitmap.anchorOffsetX = bitmap.width / 2;
        bitmap.anchorOffsetY = bitmap.height / 2;
        bitmap.x = props.x;
        bitmap.y = props.y;

        let button = new eui.Button();
        button.width = bitmap.width + 20;
        button.height = bitmap.height + 20;
        button.x = bitmap.x - 10;
        button.y = bitmap.y - 10;
        button.alpha = 0;
        button.anchorOffsetX = bitmap.anchorOffsetX;
        button.anchorOffsetY = bitmap.anchorOffsetY;
        this.addChild(button);

        return {
            bitmap: bitmap,
            button: button
        };
    }
    private createBitmap (name:string, props?) {
        const bitmap = this.createBitmapByName(name);
        if (props) {
            props.x && (bitmap.x = props.x);
            props.y && (bitmap.y = props.y);
        }
        return bitmap;
    }
    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

}