//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
const resourcePath = (window['_basePath'] || '') + 'resource' + ((window['_language'] && ('/' + window['_language'])) || '') 

class LoadingUI extends egret.Sprite implements RES.PromiseTaskReporter {

    public constructor() {
        super();
        this.createView();
    }
    
    private loadFinish = false;

    private createView(): void {
        this.onComplete();
        console.log('LoadingUI createView')
    }

    private onComplete():void {
        window['_jooxGame']['onLoadingStart'] && window['_jooxGame']['onLoadingStart']();
    }

    public onProgress(current: number, total: number): void {
        const progress = Math.floor(current/total*100);
        // console.log(current, total, progress)
        this.updateProgress(progress);
        if (progress === 100) {
            this.loadFinish = true;
            const cb = () => {
                console.log('START')
                this.dispatchEvent(new egret.Event("START", false, false, true));
            }
            setTimeout(() => {
                window['_jooxGame']['onLoadingFinish'] && window['_jooxGame']['onLoadingFinish'](cb);
                this.dispatchEvent(new egret.Event("FINISH", false, false, true));
            }, 100)
        }
    }

    private updateProgress (progress) {
        if (progress > 100) {
            return false;
        }
        window['_jooxGame']['onLoadingProgress'] && window['_jooxGame']['onLoadingProgress'](progress);

    }


}
