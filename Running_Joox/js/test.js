

var doc = document;
var url = location.href;
var wmid = encodeURIComponent(getParameter(url, "wmid"));
var openudid = encodeURIComponent(getParameter(url, "openudid"));
var key = encodeURIComponent(getParameter(url, "key").replace(/\s/g, "+"));
var area = encodeURIComponent(getParameter(url, "area"));
var lang = encodeURIComponent(getParameter(url, "lang"));


/**
 * @description get请求函数
 * @param {string} url 请求
 * @param {function} successCallback 成功回调函数
 * @param {function} errorCallback 失败回调函数
 */
 function get(url, successCallback, errorCallback) {
    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          successCallback(xmlhttp.responseText);
        } else {
          if (typeof errorCallback == "function") {
            errorCallback();
          }
        }
      }
    };
    xmlhttp.open("GET", url + "&cache=" + new Date().getTime(), true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send();
}

function getReqDomain() {
    //点击按钮
    var domains = {
      "www.joox.com": "api.joox.com",
      "test.web.joox.com": "test.api.joox.com",
      "www.joox.com": "dev.joox.com"
    };
  
    return domains[location.host] || "api.joox.com";
}

function getUserInfo () {
    var reqUrl =
      "https://" +
      getReqDomain() +
      "/oper-cgi/get_uinfo?wmid=" +
      wmid +
      "&country=" +
      area +
      "&openudid=" +
      openudid +
      "&key=" +
      key +
      "&fields=nickname"
    get(reqUrl, function (res) {
      console.log('getUserInfo', reqUrl, res, JSON.stringify(res))
      if (res.retcode == 0) {
        var reqUrl2 =
          "https://" +
          getReqDomain() +
          "/oper-cgi/get_uinfo?wmid=" +
          wmid +
          "&country=" +
          area +
          "&openudid=" +
          openudid +
          "&key=" +
          key +
          "&fields=" + res.nickname
        get(reqUrl2, function (res) {
          console.log('getUserInfo2', reqUrl2, res, JSON.stringify(res))
        }, function (err) {
            console.log('getUserInfo2 err', err)
        });
      }
    });
  }
  getUserInfo();