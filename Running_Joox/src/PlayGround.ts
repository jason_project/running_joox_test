

const gameState = {
    PENDING: 0,
    START: 1,
    PAUSE: 2,
    FAIL: 3,
    FINISH: 4
}

let tubeSpeed = 5;
let distanceX = 600;
const tubeBlockCount = 10;
const gapX = 268;
const tubeWidth = 92;
const tubeHeight = 700;
let bgmusic_canplay = false;
let screen_scale = 1;
let last_move_time = Date.now();
const move_diff = 1000 / 60;
const tube_transitions = [-571, -933, -1293, -1652, -2014, -2371, -2731, -3092, -3451, -3812, -4171, -4533, -4893, -5253, -5611];

// 产生随机数
const getRandForNum = function(min, max): number {
    return Math.round(Math.random() * (max - min) + min);
}
const getRandForFloatNum = function(min: number, max): number {
    return Math.random() * (max - min) + min;
}

const tube_num = 25

let scores = new Array(tube_num)
const tubeNote = new Array(tube_num)
  
  // 碰撞测试
const isCollide = function(player, tube, mx = 0) {
    if (typeof tube === 'undefined') return false
    // console.log(tube.i, tube.x + mx, player.x + player.bitmap.width, tube.x + mx + tube.width,player.x, mx,'\n',tube.yUpBottom, player.y, player.y + 171, tube.yDownTop)
    if (tube.x + mx < player.x + player.bitmap.width - 12 && tube.x + mx + tube.width > player.x + 2) {
       if (tube.yUpBottom - 25 > player.y || player.y + player.bitmap.height > tube.yDownTop + 25) {
        // console.log(tube.yUpBottom, player.y, player.y + player.bitmap.height, tube.yDownTop)
        return true;
      }
    }
    return false;
}
const isCollideNote = function(player, note, mx = 0, safeY) {
    if (note.x + mx - note.anchorOffsetX < player.x + player.bitmap.width - 12 && note.x - note.anchorOffsetX  + mx + note.width > player.x + 12) {
        if (note.name === 'play_ct_png') {
            return true;
        }
        // console.log(note.x + mx - note.anchorOffsetX, player.x + player.bitmap.width-5, note.x - note.anchorOffsetX  + mx + note.width , player.x + 5)
        if (note.y + safeY - note.anchorOffsetY + 15 < player.y + player.bitmap.height && note.y + safeY + note.anchorOffsetY  - 15 > player.y) {
            // console.log(note.y, note.y + safeY - note.anchorOffsetY + 5, player.y + player.bitmap.height, player.y, note.y + safeY + note.anchorOffsetY - 5 )
            return true;
        }
    }
    return false;
}

class Player {
    gravity: number;
    fallSpeed: number;
    x: number;
    y: number;
    safeY: number;
    originX: number;
    originY: number;
    rotateGravity: number;
    rotateSpeed: number;
    rotate: number;
    upDown: number;
    up: boolean;
    bitmap: egret.Bitmap;
    group: eui.Group;
    starGroup: eui.Group;
    successStarGroup: eui.Group;
    tempBitmap: egret.Bitmap;
    score: eui.Group;
    public constructor (options?) {
        this.bitmap = options.bitmap;
        this.group = options.group;
        this.starGroup = options.starGroup;
        this.successStarGroup = options.successStarGroup;
        this.tempBitmap = null;
        
        this.gravity = 0.24;
        this.fallSpeed = 0;
        this.up = false;
        this.x = options.x;
        this.y = options.y;
        this.safeY = options.safeY;
        this.originX = options.x;
        this.originY = options.y;
        this.rotateGravity = 0.12*Math.PI/180;
        this.rotateSpeed = 0;
        this.rotate= 0;
        this.upDown= 4;
    }
    
    fall () {
        this.fallSpeed += this.gravity;
        this.y += this.fallSpeed;
        if (this.y >= this.safeY - this.bitmap.height - 50) {
            this.y = this.safeY - this.bitmap.height -50;
        
        }
        if (this.tempBitmap) {
            this.tempBitmap.y = this.y
        }
        
        this.bitmap.y = this.group.y = this.y;
        if (this.score) {
            this.score.y = this.y - 30;
        }
        // console.log(this.y, this.gravity,this.fallSpeed)
        if (this.up) {
            this.rotateSpeed += this.rotateGravity;
            this.rotate += this.rotateSpeed;
            if (this.rotate <= -45*Math.PI/180) {
                this.rotate = -45*Math.PI/180
                this.up = false;
                this.rotateSpeed = 0;
            }
        }else{
            this.rotateSpeed += this.rotateGravity;
            this.rotate += this.rotateSpeed;
            if (this.rotate >= 90*Math.PI/180) {
                this.rotate = 90*Math.PI/180;
                this.rotateSpeed = 0;
            }
        }
    }
    flyup () {
        this.fallSpeed = -6.5;
        if (this.y <= -255) {
          this.y = -255;
        }
        // 上抬角度速度
        this.rotateSpeed = -10*Math.PI/180;
        this.up = true;
    }
    flyUpDown () {
        let fallSpeed = this.fallSpeed
        if (this.up) {
            fallSpeed -= 0.2;
        } else {
            fallSpeed += 0.2;
        }
        this.fallSpeed = fallSpeed
        if (Math.abs(this.fallSpeed) > this.upDown) {
            this.up = !this.up
        }
        this.bitmap.y = this.group.y = this.y += fallSpeed
        if (this.tempBitmap) {
            this.tempBitmap.y = this.y
        }
    }
    changeBitmap (bitmap) {
        this.tempBitmap = bitmap
    }
    reset () {
        this.tempBitmap = null;
        this.fallSpeed = 0;
        this.up = false;
        
        this.bitmap.x = this.group.x = this.x = this.originX;
        this.bitmap.y = this.group.y = this.y = this.originY
    }
}

class Pipe {
    i: number;
    x: number;
    countUp: number;
    countDown: number;
    yUp: number;
    yDown: number;
    yUpBottom: number;
    yDownTop: number;
    public note: egret.Bitmap | eui.Group;
    public width = tubeWidth;
    private height = tubeHeight;
    public constructor (options?) {
        let i = options.i
        this.i = options.i
        this.x = options.i * (this.width + gapX) + distanceX;
        let singleBlockHeight = this.height / tubeBlockCount
        let playHeight= options.playHeight
        
        let gap = 600;
        let random = (Math.random() > 0.5 ? -1 : 1);
        let randomStart = -2.5;
        let randomEnd = 2.5;
        if (i < 3) {
            gap = getRandForNum(750*screen_scale, 950*screen_scale);
        } else if (i < 7) {
            gap = getRandForNum(600*screen_scale, 780*screen_scale);
            // random = (Math.random() > 0.5 ? -1 : 1);
           randomStart = -1.5;
           randomEnd = 1.5;
        } else if (i < 15) {
            gap = getRandForNum(500*screen_scale, 680*screen_scale);
            random = random * Math.random() * 2;
        } else if (i < 19) {
            gap = getRandForNum(360*screen_scale, 520*screen_scale);
        } else {
            gap = getRandForNum(360*screen_scale, 450*screen_scale);
        }
        if (playHeight < 1100) {
            gap -= 60*screen_scale;
        }
        let showTubeHeight = playHeight - gap;
        let count = showTubeHeight / singleBlockHeight;
        let up = i < 3 ? 0 : count / 2 + random * getRandForFloatNum(randomStart, randomEnd);
        // if (playHeight < 1100) {
        //     random = (Math.random() > 0.5 ? -1 : 0);
        //     randomStart = 0.8;
        // }
        if (playHeight < 1100) {
            if (up > 1.5) {
                up -= 0.8;
            }
        }
        // up > 4 && (up = getRandForFloatNum(3, 4));
        // up < 1 && (up = getRandForFloatNum(0, 1));
        let down = count - up;
        if (down < 2) {
            down = 2;
            up = count - down;
        }
        // down > 4 && (down = getRandForFloatNum(3, 4));
        this.countUp = up;
        this.countDown = down;

        let safeY = options.safeY;
        let baseY = options.baseY;
        let countUp = this.countUp;
        let countDown = this.countDown;
        // console.log(this.countUp, this.countDown, baseY,  countUp * singleBlockHeight, singleBlockHeight)
        this.yUp = baseY - (tubeBlockCount - countUp) * singleBlockHeight;
        this.yDown = baseY + playHeight - countDown * singleBlockHeight;
        this.yUpBottom = safeY + baseY + countUp* singleBlockHeight;
        this.yDownTop = safeY + this.yDown;
    }
}

class PlayGround extends egret.DisplayObjectContainer {
    duration: number;
    score: number;
    musicOn: boolean;
    gameState: number;

    countTime: egret.TextField[];

    player: Player;
    invinciblePlayerGroup: eui.Group;
    starGroup: eui.Group;
    tubeGroup: eui.Group;
    noteGroup: eui.Group;
    bgGroup: eui.Group;
    cloudGroup: eui.Group;
    successDoorGroup: eui.Group;
    successDoorTop: egret.Bitmap;
    successDoorTopX: number;
    tubeArray: Array<Pipe>;

    endX: number;
    bonusset: number;
    bonusIndex: number;
    invincible: number;
    finishNum: number;

    public constructor (options?) {
        super();
        this.score = 0;
        if (options) {
            this.musicOn = options.musicOn || false;
        }
        this.gameState = gameState.PENDING;

        this.tubeArray = [];
        this.finishNum = 0;
        this.bonusset = 0;
        this.bonusIndex = 15;
        // this.bonusIndex = getRandForNum(15, 18);
        // this.bonusIndex = getRandForNum(1, 2);
        this.invincible = 0;
        
        this.once(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);
        

    }

    private soundChannels: egret.SoundChannel [] = [];
    private scoreBar: ScoreBar;
    private successBg: egret.Shape;

    private props = {
        headerBarHeight: 100,
        headerHeight: 146,
        safeX: 0,
        safeY: 0,
        safeWidth: 0,
        safeHeight: 0,
        ratioX: 0,
        ratio: 0,
        scale: 1,
    }

    public initStageProps () {
        let initStageProps = window['_initStageProps'];
        this.props = initStageProps();
        screen_scale = this.props.scale;
        if (screen_scale > 1) {
            screen_scale = 1;
        }
    }
    
    private onAddToStage () {
        // this.stage.scaleMode = egret.StageScaleMode.NO_BORDER
        this.initStageProps();
        
        this.initBg();
        this.initTube();
        this.renderSuccessBg();
        this.initNote();
        this.renderPlayer();

        this.initPreCountDown(() => {
            this.renderTapHandle();
            setTimeout(() =>{
                if (this.gameState === gameState.PENDING) {
                    this.gameState = gameState.START;
                }
            }, 50)
            this.scoreBar.shakeProgressVip();
        })
        this.renderScoreBar();

        egret.ticker.$startTick(this.update, this);
    }

    private update ():boolean {
        if (this.gameState === gameState.PENDING) {
            this.player.flyUpDown();
        } else if (this.gameState === gameState.START) {
            this.moveTube();
        } else if (this.gameState === gameState.FAIL) {
            this.player.fall();
        }
        return true;
    }

    private onGameOver (end?:boolean) {
        const fun = (cb) => {
            cb && cb(this.score);
        }
        if (end) {
            this.showSuccessVip()
        }
        window['_jooxGame']['onGameOver'] && window['_jooxGame']['onGameOver'](end ? fun : null, end ? null : fun);
    }

    private moveTube () {
        let finishNum = this.finishNum
        const tube = this.tubeArray[finishNum]
        
        if (typeof tube === 'undefined') {
            tubeSpeed += 4;
        } else {
            if (this.invincible) {
                tube.i > tube_num - 4 ? (tubeSpeed > 50 ? tubeSpeed -= 6 : tubeSpeed -= 3) : tubeSpeed += 2;
                if (tubeSpeed < 15) {
                    tubeSpeed = 15;
                }
                if (tubeSpeed > 100) {
                    tubeSpeed = 100;
                }
            } else {
                const move_time = Date.now();
                tubeSpeed = move_time - last_move_time < move_diff ? 3 : 5;
                last_move_time = move_time;
            }
        }
        if (this.player.bitmap.x > this.player.group.x) {
            this.player.bitmap.x -= tubeSpeed * 1.5;
            if (this.player.bitmap.x < this.player.group.x) {
                this.player.bitmap.x = this.player.group.x;
            }
        }
        this.tubeGroup.x -= tubeSpeed;
        this.noteGroup.x -= tubeSpeed;
        this.bgGroup.x -= tubeSpeed;
        this.cloudGroup.x -= tubeSpeed;
        this.successDoorGroup.x -= tubeSpeed;
        this.successDoorTop.x -= tubeSpeed;
        
        if (!this.invincible && this.player.bitmap.x == this.player.group.x) {
            this.player.fall();
        }
        
        if (typeof tube === 'undefined') {
            // console.log(tubeSpeed, this.successDoorGroup.x, this.successDoorTop.x)
            if (this.successDoorGroup.x < -180) {
                this.gameState = gameState.FINISH;
                this.onGameOver(true);
            }
            return;
        }
        // 碰撞测试
        if (!this.invincible && (!scores[tube_num - 1])) {
            if (isCollide(this.player, tube, this.tubeGroup.x) || 
                isCollide(this.player, this.tubeArray[finishNum+1], this.tubeGroup.x)) {
                // this.createSound('bgmusic_fail_mp3')
                this.gameState = gameState.FAIL;
                this.failGame();
            }
        }
        
        if (!this.invincible) {
            
            if (tube.note && (isCollideNote(this.player, tube.note, this.tubeGroup.x, this.props.safeY))) {
                // console.log(tube.note.name)
                let score = 0;
                let scoreX = this.player.x + this.player.bitmap.width / 2;
                switch (tube.note.name) {
                    case 'play_yf_m3_png':
                        this.createSound('music_get10_mp3');
                        this.renderTempPlayerBitmap('play_jojo_png');
                        score = 10;
                        break;
                    case 'play_yf+10_png':
                        this.createSound('music_get10_mp3');
                        this.renderTempPlayerBitmap('play_jojo_png');
                        score = 10;
                        break;
                    case 'play_yf+30_png':
                        this.createSound('music_get10_mp3');
                        this.renderTempPlayerBitmap('play_jojo+30_png');
                        score = 30;
                        break;
                    case 'play_ct_png':
                        this.createSound('music_citie_mp3');
                        this.showSuccessBg();
                        this.renderInvinciblePlayerBitmap();
                        score = 10;
                        tubeSpeed = 25;
                        this.invincible = 1;
                        break;
                }
                if (score > 0) {
                    scores[finishNum] = score
                    this.flyScore(score, scoreX, this.player.y)
                }
                this.score += score
                this.player.bitmap.alpha = 0
                this.noteGroup.removeChild(tube.note);
                tube.note = null;
            }
            //得分音效
            if (Math.abs(Math.abs(tube.x + this.tubeGroup.x) + tube.width - this.player.x) < tubeSpeed / 2 || this.tubeGroup.x < tube_transitions[finishNum]) {
                let scoreX = this.player.x + this.player.bitmap.width / 2
                let score = 0;
                if (tube.i == this.bonusIndex) {
                    this.createSound('music_citie_mp3');
                    this.showSuccessBg();
                    this.renderInvinciblePlayerBitmap();
                    score = 10;
                    tubeSpeed = 25;
                    this.invincible = 1;
                    scores[finishNum] = score;
                    
                    this.player.bitmap.alpha = 0;
                    this.adsorbNote(tube.note, this.tubeGroup.x, () => {
                        tube.note = null;
                    });
                }
                // console.log('finishNum', finishNum, scores[finishNum], !scores[finishNum], tube.x, this.tubeGroup.x )
                if (!scores[finishNum]) {
                    score = 1;
                    scores[finishNum] = score;
                }
                if (score > 0) {
                    this.score += score
                    this.flyScore(score, scoreX, this.player.y);
                }
                finishNum++
                this.finishNum = finishNum
            };
        }
        // if (tubeSpeed < 100) {
            // console.log(this.finishNum, Math.abs(Math.abs(tube.x + this.tubeGroup.x) + tube.width - this.player.x),  tubeSpeed / 2, tubeSpeed)
        // }
        if (this.invincible &&  Math.abs(Math.abs(tube.x + this.tubeGroup.x) + tube.width - this.player.x) < tubeSpeed) {
            // console.log(tube)
            let scoreX = this.player.x + this.player.bitmap.width / 2
            let score = 0;
            if (tube.note) {
                switch (tube.note.name) {
                    case 'play_yf_m3_png':
                        score = 10;
                        break;
                    case 'play_yf+10_png':
                        score = 10;
                        break;
                    case 'play_yf+30_png':
                        score = 30;
                        break;
                }
                if (finishNum == tube_num - 2) {
                    this.hideSuccessBg();
                    this.hideTapButton();
                }
                if (finishNum == tube_num - 1) {
                    this.removeChild(this.invinciblePlayerGroup);
                    this.renderSuccessVip();
                    this.renderSuccessPlayerBitmap();
                }
                scores[finishNum] = score;
                this.adsorbNote(tube.note, this.tubeGroup.x, () => {
                    tube.note = null;
                });
            }
            if (score > 0) {
                this.score += score
                this.flyScore(score, scoreX, this.player.y - this.player.bitmap.height / 2 - 20);
            }
            finishNum++
            this.finishNum = finishNum
        };

        this.updateScore(this.finishNum);
    }

    private renderScoreBar () {
        const scoreBar = new ScoreBar({
            musicOn: this.musicOn
        });
        this.scoreBar = scoreBar;
        scoreBar.addEventListener('TOGGLE_MUSIC', () => {
            console.log('TOGGLE_MUSIC')
            this.musicOn = !this.musicOn;
            
            if (bgmusic_canplay) {
                this.dispatchEvent(new egret.Event("TOGGLE_MUSIC", false, false, true))
            }
        }, this)
        scoreBar.addEventListener('PLAY_MUSIC', () => {
            this.dispatchEvent(new egret.Event("PLAY_MUSIC", false, false, true));
        }, this)
        scoreBar.addEventListener('STOP_MUSIC', () => {
            console.log('STOP_MUSIC')
            this.dispatchEvent(new egret.Event("STOP_MUSIC", false, false, true));
        }, this)
        this.stage.addChild(scoreBar);
    }

    private updateScore (index) {
        this.scoreBar.updateScore(this.score, index);
    }

    private renderSuccessBg () {
        const maskBg = new egret.Shape();
        maskBg.x = 0;
        maskBg.y = 0;
        maskBg.width = this.stage.stageWidth
        maskBg.height = this.stage.stageHeight;
        maskBg.alpha = 0;
        maskBg.graphics.beginFill(0x000000);
        maskBg.graphics.drawRect(0, 0, maskBg.width, maskBg.height);
        maskBg.graphics.endFill();
        // this.addChild(maskBg);
        this.addChildAt(maskBg, 0);
        this.successBg = maskBg;
    }

    private showSuccessBg () {
        const maskBg = this.successBg;
        this.setChildIndex(maskBg, 11);
        this.setChildIndex(this.noteGroup, 11);
        const tw = egret.Tween.get(maskBg);
        tw.to({ "alpha": 0.6 }, 200)
    }

    private hideSuccessBg () {
        const maskBg = this.successBg;
        const tw = egret.Tween.get(maskBg);
        tw.to({ "alpha": 0 }, 300)
    }

    private hideTapButton: Function;

    private renderPlayer () {
        const props = this.props;
        let {scale} = props;
        if (scale > 1) scale = 1;
        const { safeX, safeY, safeHeight, ratio } = props
        const group = new eui.Group();
        const x = safeX + 120;
        const y = this.$stage.stageHeight / 2 - 200;
        group.x = x;
        group.y = y;
        const bitmap = this.createBitmap('play_jojo_png', {
            x: x,
            y: y
        })
        bitmap.width = bitmap.width * scale;
        bitmap.height = bitmap.height * scale;
        bitmap.x = this.stage.stageWidth / 2 - bitmap.width / 2;
        const star1 = this.createBitmap('play_star_png', {
            x: bitmap.width - 15 * ratio,
            y: -2 * ratio
        })
        star1.width = star1.width;
        star1.height = star1.height;
        const star2 = this.createBitmap('play_star_png', {
            x: -5 * ratio,
            y: bitmap.height * 2 / 3 + 5 * ratio
        })
        star2.width = star2.width * 2 / 3;
        star2.height = star2.height * 2 / 3;
        const starGroup = new eui.Group();
        starGroup.x = 0;
        starGroup.y = 0;
        starGroup.addChild(star1);
        starGroup.addChild(star2);
        starGroup.alpha = 0;

        this.player = new Player({
            x: x,
            y: y,
            bitmap: bitmap,
            group: group,
            starGroup: starGroup,
            safeY: safeY + safeHeight
        })
        // console.log(this.player)
        group.addChild(starGroup);
        this.addChildAt(group, 5);
        this.addChildAt(bitmap, 5);
    }
    private renderInvincibleStar (group, bitmap) {
        const props = this.props;
        const { ratio } = props
        const starGroup = new eui.Group();
        starGroup.x = 0;
        starGroup.y = 0;
        const star1 = this.createBitmap('play_star_png', {
            x: bitmap.width / 2 - 12 * ratio,
            y: -bitmap.height / 2 + 5 * ratio
        })
        star1.width = star1.width;
        star1.height = star1.height;
        const star2 = this.createBitmap('play_star_png', {
            x: -bitmap.width / 2 - 5 * ratio,
            y: bitmap.height / 2 * 2 / 3 
        })
        star2.width = star2.width * 2 / 3;
        star2.height = star2.height * 2 / 3;
        starGroup.x = 0;
        starGroup.y = 0;
        starGroup.addChild(star1);
        starGroup.addChild(star2);
        group.addChild(starGroup);
    }
    private renderSuccessStar (bitmap) {
        const props = this.props;
        const { ratio } = props
        const successStarGroup = new eui.Group();
        successStarGroup.x = bitmap.x;
        successStarGroup.y = bitmap.y;
        const star1 = this.createBitmap('play_star_png', {
            x: bitmap.width - 12 * ratio,
            y: 10 * ratio
        })
        const star2 = this.createBitmap('play_star_png', {
            x: bitmap.width  / 2 - 20 * ratio,
            y: bitmap.height / 2 + 10 * ratio
        })
        star2.width = star2.width / 2;
        star2.height = star2.height / 2;
        const star3 = this.createBitmap('play_star_png', {
            x: bitmap.width - 15 * ratio,
            y: bitmap.height - 5* ratio
        })
        star3.width = star3.width / 2;
        star3.height = star3.height / 2;
        successStarGroup.addChild(star1);
        successStarGroup.addChild(star2);
        successStarGroup.addChild(star3);
        return successStarGroup;
    }
    private renderTempPlayerBitmap (name) {
        let {scale} = this.props;
        if (scale > 1) scale = 1;
        const player = this.player;
        const bitmap = this.createBitmap(name, {
            x: player.x,
            y: player.y
        })
        bitmap.width = bitmap.width * scale;
        bitmap.height = bitmap.height * scale;
        bitmap.width = bitmap.width;
        bitmap.height = bitmap.height;
        // bitmap.anchorOffsetX = (bitmap.width - player.bitmap.width) / 2;
        // bitmap.anchorOffsetY = (bitmap.height - player.bitmap.height) / 2;
        player.starGroup.alpha = 1;
        this.removeTempPlayerBitmap()
        this.player.changeBitmap(bitmap)
        this.addChild(bitmap);
        let tw = egret.Tween.get(bitmap);
        tw.wait(300);
        tw.call(() => {
            this.removeTempPlayerBitmap()
            this.player.bitmap.alpha = 1
            player.starGroup.alpha = 0;
        }, this);
    }
    private renderInvinciblePlayerBitmap () {
        const player = this.player;
        const group = new eui.Group();
        group.x = player.x + player.bitmap.width / 2;
        group.y = player.y + player.bitmap.height / 2;
        const light = this.createBitmap('play_jojo+ct_light_png', {
            x: 0,
            y: 0
        })
        light.width = light.width;
        light.height = light.height;
        light.anchorOffsetX = light.width / 2;
        light.anchorOffsetY = light.height / 2;
        light.rotation = 0;
        const bitmap = this.createBitmap('play_jojo_png', {
            x: 0,
            y: 0
        })
        bitmap.width = bitmap.width * 4 / 3;
        bitmap.height = bitmap.height * 4 / 3;
        bitmap.anchorOffsetX = bitmap.width / 2;
        bitmap.anchorOffsetY = bitmap.height / 2;
        group.addChild(light);
        group.addChild(bitmap);
        this.renderInvincibleStar(group, bitmap);
        this.player.changeBitmap(bitmap);
        this.addChild(group);
        this.invinciblePlayerGroup = group;
        this.rotateInvinciblePlayerLight(light, bitmap);
    }
    private renderSuccessPlayerBitmap () {
        const player = this.player;
        const bitmap = this.createBitmap('play_jojo_win_png', {
            x: player.x,
            y: this.stage.stageHeight / 2 - player.bitmap.height / 2
        })
        bitmap.width = bitmap.width;
        bitmap.height = bitmap.height;
        // bitmap.anchorOffsetX = bitmap.width / 2;
        // bitmap.anchorOffsetY = bitmap.height / 2;
        this.addChild(bitmap);
        const successStarGroup = this.renderSuccessStar(bitmap);
        this.addChild(successStarGroup);
        this.setChildIndex(this.successDoorTop, 1000);
        let tw = egret.Tween.get(bitmap);
        let tw2 = egret.Tween.get(successStarGroup);
        tw.to({ x: bitmap.x + 80 }, 200);
        tw2.to({ x: bitmap.x + 80 }, 200);
    }
    
    private rotateInvinciblePlayerLight (result: egret.Bitmap, bitmap: egret.Bitmap) {
        let go = () => {
            let tw = egret.Tween.get(result);
            tw.to({ "rotation": 360 }, 1500);
            result.rotation = 0;
            tw.call(go, this);
        }
        go();
        let up = false
        const group = this.invinciblePlayerGroup;
        const successStarGroup = this.player.group;
        let y = this.stage.stageHeight / 2;
        // this.player.y = this.player.bitmap.y = y;
        // let y = group.y;
        this.player.y = this.player.bitmap.y = y;
        let fallSpeed = 0;
        const upDown = 8;
        let go1 = () => {
            let tw = egret.Tween.get(group);
            let tw2 = egret.Tween.get(successStarGroup);
            let tw3 = egret.Tween.get(this.player);
            if (up) {
                fallSpeed -= 1;
            } else {
                fallSpeed += 1;
            }
            if (Math.abs(fallSpeed) > upDown) {
                up = !up;
            }
            y += fallSpeed;
            tw.to({ y: y }, 16);
            tw2.to({ y: y }, 16);
            tw3.to({ y: y }, 16);
            tw.call(go1, this);
        }
        go1();
    }

    private removeTempPlayerBitmap () {
        if (this.player.tempBitmap) {
            this.removeChild(this.player.tempBitmap);
            this.player.tempBitmap = null;
        }
    }

    
    private flyScore (num:number ,x:number, y:number) {
        const group = new eui.Group();
        group.x = num > 1 ? x -10 : x;
        group.y = y - 30;
        const text:egret.TextField = new egret.TextField();
        text.textColor = num == 30 ? 0xffe516 : 0xffb42d;
        text.size = 60;
        text.fontFamily = 'Joox Brand Bold';
        text.text = num.toString();
        text.stroke = 3;
        text.strokeColor = 0x000000;
        text.verticalAlign = egret.VerticalAlign.MIDDLE;
        text.x = 20;
        text.y = 0;
        text.anchorOffsetY = text.height / 2;
        group.addChild(text);
        
        const plus:egret.TextField = new egret.TextField();
        plus.textColor = text.textColor;
        plus.size = text.size - 8;
        plus.fontFamily = text.fontFamily;
        plus.text = '+';
        plus.stroke = text.stroke;
        plus.strokeColor = text.strokeColor;
        plus.verticalAlign = egret.VerticalAlign.MIDDLE;
        plus.x = -20;
        plus.y = - (window['isIos'] ? 7 : 4);
        plus.anchorOffsetY = plus.height / 2;
        group.addChild(plus);
        this.addChild(group);
        if ( this.invincible ) {
            let tw = egret.Tween.get(group);
            tw.to({ "y": group.y - 30}, 100, egret.Ease.cubicInOut);
            tw.call(() => {
                let tw1 = egret.Tween.get(group);
                tw1.to({ alpha: 0 }, 100, egret.Ease.cubicOut);
                tw1.call(() => {
                    this.removeChild(group);
                }, this);
            }, this);
        } else {
            this.player.score = group;
            let timer = setTimeout(() => {
                this.removeChild(group);
                this.player.score = null;
                clearTimeout(timer);
            }, 300)
        }
    }

    private renderTapHandle () {
        const props = this.props;
        const { ratioX: ratio, safeY, safeHeight, headerBarHeight } = props
        
        const group = new eui.Group();
        group.x = this.$stage.stageWidth / 2
        group.y = safeY + safeHeight - 100 * ratio
        
        const radius = 180 / 2 * ratio / 2;
        let circle = new egret.Shape();
        circle.width = radius * 2;
        circle.height = radius * 2;
        circle.x = 0;
        circle.y = 1 * ratio;
        circle.graphics.beginFill(0x000000);
        circle.graphics.lineStyle(4, 0x000000);
        circle.graphics.drawCircle(0, 0, radius);
        circle.graphics.endFill();
        let circle2 = new egret.Shape();
        circle2.width = radius * 2;
        circle2.height = radius * 2;
        circle2.x = 0;
        circle2.y = 0;
        circle2.graphics.beginFill(0xfff72a);
        circle2.graphics.lineStyle(4, 0x000000);
        circle2.graphics.drawCircle(0, 0, radius);
        circle2.graphics.endFill();
        
        group.addChild(circle);
        group.addChild(circle2);
        
        const btnText:egret.TextField = new egret.TextField();
        btnText.textColor = 0x000000;
        btnText.size = (window['myTexts']['tapFS'] || 18) * ratio;
        btnText.bold = typeof window['myTexts']['tapBold'] == 'undefined' ? true : window['myTexts']['tapBold'];
        btnText.fontFamily = window['myTexts']['tapFF'] || 'PingFang SC, helvetica neue, Microsoft YaHei';
        btnText.text = window['myTexts']['tap'];
        btnText.x = 0;
        btnText.y = 0;
        btnText.anchorOffsetX = btnText.width / 2;
        btnText.anchorOffsetY = btnText.height / 2;
        group.addChild(btnText);

        let button = new eui.Button();
        button.width = this.$stage.stageWidth;
        button.height = safeHeight - headerBarHeight;
        button.x = this.$stage.stageWidth / 2;
        button.y = safeY + headerBarHeight;
        // button.alpha = 0;
        button.anchorOffsetX = button.width / 2;
        // button.anchorOffsetY = button.height / 2;
        
        button.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            // console.log('TAP')
            if (this.gameState !== gameState.START || this.invincible) return false;
            this.player.flyup();
            let tw = egret.Tween.get(circle);
            let tw2 = egret.Tween.get(circle2);
            let tw1 = egret.Tween.get(btnText);
            tw.to({"scaleX": 1, "scaleY": 1}, 100);
            tw1.to({"scaleX": 1, "scaleY": 1}, 100);
            tw2.to({"scaleX": 1, "scaleY": 1}, 100);
        }, this);
        button.addEventListener(egret.TouchEvent.TOUCH_BEGIN, () => {
            let tw = egret.Tween.get(circle);
            let tw2 = egret.Tween.get(circle2);
            let tw1 = egret.Tween.get(btnText);
            tw.to({"scaleX": 0.9, "scaleY": 0.9}, 100);
            tw1.to({"scaleX": 0.9, "scaleY": 0.9}, 100);
            tw2.to({"scaleX": 0.9, "scaleY": 0.9}, 100);
        }, this);

        this.addChildAt(group, 9);
        this.addChildAt(button, 10);
        this.hideTapButton = () => {
            const tw = egret.Tween.get(group);
            tw.to({ "alpha": 0 }, 300)
        }
    }

    private initBg (cb?:Function) {
        const props = this.props;
        const { safeY, headerBarHeight, safeHeight, scale } = props
        const group = new eui.Group();
        const preWidth = distanceX - gapX / 2;
        const baseWidth = (tubeWidth + gapX) * 5;
        let width = preWidth;
        const height = safeHeight - headerBarHeight;
        this.bgGroup = group;
        this.resetBg();
        group.y = safeY + headerBarHeight;
        const self = this;

        const bgColors = [0x3583fd, 0x3583fd, 0x3583fd, 0x3583fd, 0x3583fd, 0x3583fd, 0x3583fd]
        // const bgColors = [0x3583fd, 0x8652ff, 0x5381FF, 0x2DA6D5, 0x3FACD8, 0x4CB4DE, 0xffc12d]
        for (let i = 0; i < 7; i++) {
            (function (index) {
                const bgColor = bgColors[index];
                if (index > 0) {
                    width = baseWidth;
                }
                const x = index == 0 ? 0 : preWidth + (index - 1) * width;
                const time = 0;
                // console.log(index, preWidth, width, x)
                setTimeout(() => {
                    const bg = new egret.Shape();
                    bg.x = x;
                    bg.y = 0;
                    bg.width = width;
                    bg.height = height;
                    bg.graphics.beginFill(bgColor);
                    bg.graphics.drawRect(0, 0, width, height);
                    bg.graphics.endFill();
                    group.addChild(bg);
                    if (index == 6) {
                        self.endX = x;
                        self.renderBgSpots(x, height);
                        self.renderBgCloud(x, height);
                        self.renderSuccessDoor(x, height);
                    }
                }, time)
            })(i);
        }
        this.addChild(group);
    }

    private resetBg () {
        // const group = this.bgGroup;
        this.bgGroup.x = 0;
        // group.x = distanceX - gapX / 2;
    }

    private showSuccessVip: Function;


    private renderBgSpots (width: number, height: number) {
        const bgGroup = this.bgGroup;
        const spotBgWidth = 10;
        const spotImg = 'play_bg_spots_png';
        for (let i = 0; i < width / spotBgWidth; i++) {
            const x = spotBgWidth * i;
            const up = this.createBitmap(spotImg, {
                x: x,
                y: 0
            })
            up.y = up.height / 2;
            up.anchorOffsetY = up.height / 2;
            up.rotation = 180;
            bgGroup.addChild(up);
            const down = this.createBitmap(spotImg, {
                x: x,
                y: height - up.height
            })
            bgGroup.addChild(down);
        }
    }

    private renderBgCloud (width: number, height: number) {
        const bgGroup = this.bgGroup;
        const group = new eui.Group();
        group.x = bgGroup.x;
        group.y = bgGroup.y;
        const cloudWidth = 928;
        const cloudImg = 'play_cloud_png';
        for (let i = 0; i < width / cloudWidth; i++) {
            const x = cloudWidth * i;
            const up = this.createBitmap(cloudImg, {
                x: x + cloudWidth / 2,
                y: 0
            })
            up.y = up.height / 2;
            up.anchorOffsetX = up.width / 2;
            up.anchorOffsetY = up.height / 2;
            up.rotation = 180;
            group.addChild(up);
            const down = this.createBitmap(cloudImg, {
                x: x,
                y: height - up.height
            })
            group.addChild(down);
        }
        this.cloudGroup = group;
        this.addChild(group);
    }

    private renderSuccessDoor (x: number, height: number) {
        const bgGroup = this.bgGroup;
        const success_end_group = new eui.Group();
        success_end_group.x = x;
        success_end_group.y = bgGroup.y;
        const play_door_bottom = this.createBitmap('play_door_bottom_png', {
            x: 0,
            y: 0
        })
        // const play_door_bottom_width = play_door_bottom.width;
        // const play_door_bottom_height = play_door_bottom.height;
        const ratio = height / 1088;
        play_door_bottom.height = height;
        play_door_bottom.width = 1000;
        play_door_bottom.x = 75 * ratio;
        success_end_group.addChild(play_door_bottom);
        
        const coverBg = new egret.Shape();
        coverBg.x = play_door_bottom.x;
        coverBg.y = 0;
        coverBg.width = 180 * ratio;
        coverBg.height = height;
        coverBg.graphics.beginFill(0xffc12d);
        coverBg.graphics.drawRect(0, 0, coverBg.width, coverBg.height);
        coverBg.graphics.endFill();
        success_end_group.addChild(coverBg);

        const play_door_middle= this.createBitmap('play_door_middle_png', {
            x: -40 * ratio,
            y: 0
        })
        const play_door_middle_width = play_door_middle.width;
        const play_door_middle_height = play_door_middle.height;
        play_door_middle.height = height;
        play_door_middle.width = play_door_middle_width / play_door_middle_height * height;
        success_end_group.addChild(play_door_middle);
        const play_door_top = this.createBitmap('play_door_top_png', {
            x: x - 173 * ratio,
            y: success_end_group.y
        })
        const play_door_top_width = play_door_top.width;
        const play_door_top_height = play_door_top.height;
        play_door_top.height = height;
        play_door_top.width = play_door_top_width / play_door_top_height * height;

        // console.log(x, play_door_top.x, play_door_top.y, play_door_middle.x,  play_door_bottom.x, play_door_middle.width, play_door_top.width)
        this.successDoorGroup = success_end_group;
        this.successDoorTop = play_door_top;
        this.successDoorTopX = play_door_top.x;
        this.addChild(play_door_top);
        this.addChild(success_end_group);
    }

    private renderSuccessVip () {
        const { scale } = this.props;
        const group = new eui.Group();
        group.x = 0;
        group.y = this.$stage.stageHeight / 2;
        const vip1 = this.createBitmap('play_door_vip_png', {
            x: 0,
            y: -50 * scale
        })
        vip1.width = vip1.width;
        vip1.height = vip1.height;
        vip1.anchorOffsetY = vip1.height / 2;
        const vip2 = this.createBitmap('play_door_vip_png', {
            x: 100 * scale,
            y: -50 * scale
        })
        vip2.width = vip2.width / 3;
        vip2.height = vip2.height / 3;
        vip2.anchorOffsetY = vip2.height / 2;
        vip2.rotation = 40;
        const vip3 = this.createBitmap('play_door_vip_png', {
            x: 80,
            y: 180
        })
        vip3.width = vip3.width * 4 / 5;
        vip3.height = vip3.height * 4 / 5;
        vip3.anchorOffsetX = vip3.width / 2;
        vip3.anchorOffsetY = vip3.height / 2;
        vip3.rotation = 50;
        vip1.alpha = vip2.alpha = vip3.alpha = 0;
        group.addChild(vip1);
        group.addChild(vip2);
        group.addChild(vip3);
        this.addChild(group);
        
        let isGo = false;
        let go = () => {
            if (isGo) return false;
            isGo = true;
            // console.log('renderSuccessVip', group.x, group.y)
            let tw1 = egret.Tween.get(vip1);
            let tw2 = egret.Tween.get(vip2);
            let tw3 = egret.Tween.get(vip3);
            // let tw4 = egret.Tween.get(this.tubeGroup);
            // let tw5 = egret.Tween.get(this.bgGroup);
            tw1.to({ x: 260 * scale, y: -150 * scale, alpha: 0.9 }, 500);
            tw2.to({ x: 520 * scale, y: 0 * scale, alpha: 0.6 }, 400);
            tw3.to({ x: 260 * scale, y: 260 * scale, alpha: 0.8 }, 500);
            // tw4.to({ x: this.tubeGroup.x - 300 }, 300);
            // tw5.to({ x: this.bgGroup.x - 300 }, 300);
            
        }
        this.showSuccessVip = go;
    }

    private initTube () {
        const props = this.props;
        const { safeX, safeY, safeHeight } = props
        const group = new eui.Group();
        group.x = safeX;
        group.y = safeY;
        // group.alpha = 0;
        this.addChild(group);
        this.tubeGroup = group
        for (let i = 0; i < tube_num; i++) {
            const pipe:Pipe = new Pipe({
                safeY: safeY,
                baseY: 110,
                playHeight: safeHeight - 110,
                i
            })
            this.tubeArray.push(pipe)
        };
        // console.log(this.tubeArray)
        for (let i = 0; i < tube_num; i++) {
            this.renderTube(this.tubeArray[i])
        };
    }

    private initNote () {
        const props = this.props;
        const { safeX, safeY } = props
        const group = new eui.Group();
        group.x = safeX;
        group.y = safeY;
        // group.alpha = 0;
        this.addChild(group);
        this.noteGroup = group
        for (let i = 0; i < tube_num; i++) {
            this.renderNote(this.tubeArray[i])
        };
    }

    private renderTube (obj) {
        let tubename = (Math.floor(obj.i / 5) + 1) % 2 == 0 ? 'play_wall_purple_png' : 'play_wall_yellow_png';
        // let tubename = Math.floor(obj.i / tube_num) >= 6 && obj.i < 13 ? 'play_wall_pink_png' : 'play_wall_yellow_png'
        if (obj.countUp > 0) {
            const upTube = this.createBitmap(tubename, {
                x: obj.x,
                y: obj.yUp
            })
            upTube.width = tubeWidth;
            upTube.height = tubeHeight;
            this.tubeGroup.addChild(upTube);
        }
        if (obj.countDown > 0) {
            const downTube = this.createBitmap(tubename, {
                x: obj.x,
                y: obj.yDown
            })
            downTube.width = tubeWidth;
            downTube.height = tubeHeight;
            this.tubeGroup.addChild(downTube);
        }


    }

    private renderNote (obj) {
        const props = this.props;
        const { ratioX: ratio, safeHeight} = props
        const minY = 150 * ratio;
        const maxY = safeHeight - 200 * ratio;

        let bonusIndex = this.bonusIndex;
        let notename = (obj.i < bonusIndex && (obj.i + 1) % 5 == 0) || (obj.i > bonusIndex && obj.i % 2 == 0) ? 'play_yf+30_png' : 'play_yf+10_png';
        if (window['_language'] == 'th' && obj.i < bonusIndex && (obj.i + 1) % 5 == 3) {
            notename = 'play_yf_m3_png';
        }
        // console.log(obj.i, bonusIndex, !this.bonusset)
        if (obj.i === bonusIndex && !this.bonusset) {
            this.bonusset = 1;
            notename = 'play_ct_png';
        }
        let gap = obj.yDownTop - obj.yUpBottom;
        let min = gap / 2 - 110 > 110 ? 110 : 70;
        let max = gap / 2 - 40;
        let random = Math.random() > 0.5 ? -1 : 1
        if (obj.i  > 0 && obj.i != bonusIndex) {
            // if (Math.random() > 0.5) {
                random = -tubeNote[obj.i-1].random;
            // }
        }
        if (obj.i < 3) {
            // random = random < 0 ? -random : random;
            // console.log(random)
        }
        let tranlate_x = 0;
        if (obj.i > 12) {
            tranlate_x = Math.random() > 0.6 ? (Math.random() > 0.5 ? -1 : 1) * getRandForNum(0, 100) : 0;
        } 
        let y = obj.yDown - gap / 2 + random * getRandForNum(min, max);
        if (y < minY) {
            y = minY;
        }
        if (y > maxY) {
            y = maxY;
        }
        if (notename === 'play_ct_png') {
            random = -tubeNote[obj.i-1].random;
            y = obj.yDown - gap / 2 + random * getRandForNum(min, max - 60);
            // y = obj.yDown - gap / 2 + random * getRandForNum(150, 300);
            tranlate_x =  (Math.random() > 0.5 ? -1 : 1) * getRandForNum(0, 100);
        } else {
            // if (Math.abs(tranlate_x) > 50) {
            //     y = random > 0 ? y + 50 : y - 50
            // }
        }

        tubeNote[obj.i] = {
            random: random
        }
        const note = this.createBitmap(notename, {
            x: obj.x + obj.width / 2 + tranlate_x,
            y: y
        })
        note.name = notename;
        note.width = note.width;
        note.height = note.height;
        note.anchorOffsetX = note.width / 2;
        note.anchorOffsetY = note.height / 2;
        note.name = notename;
        obj.note = note;
        this.noteGroup.addChild(note);
        if (notename === 'play_ct_png') {
            const star1 = this.createBitmap('play_star_png', {
                x: note.x + note.anchorOffsetX ,
                y: note.y + note.anchorOffsetY - 16
            })
            star1.width = star1.width / 2;
            star1.height = star1.height / 2;
            star1.anchorOffsetX = star1.width * 2 / 3;
            star1.anchorOffsetY = star1.height * 2 / 3;
            const star2 = this.createBitmap('play_star_png', {
                x: note.x - note.anchorOffsetX + 16,
                y: note.y - note.anchorOffsetY + 16
            })
            star2.anchorOffsetX = star2.width / 2;
            star2.anchorOffsetY = star2.height / 2;
            this.noteGroup.addChild(star1);
            this.noteGroup.addChild(star2);
            let go1 = () => {
                let tw = egret.Tween.get(star1);
                tw.to({ "scaleX": 1.3, "scaleY": 1.3 }, 300);
                tw.wait(100);
                tw.to({ "scaleX": 1, "scaleY": 1 }, 300);
                tw.wait(100);
                tw.call(go1, this);
            }
            let go2 = () => {
                let tw = egret.Tween.get(star2);
                tw.to({ "scaleX": 1.3, "scaleY": 1.3 }, 350);
                tw.wait(100);
                tw.to({ "scaleX": 1, "scaleY": 1 }, 350);
                tw.wait(100);
                tw.call(go2, this);
            }
            go1();
            go2();
        }
    }

    private resetNotes () {
        this.noteGroup.removeChildren()
        for (let i = 0; i < tube_num; i++) {
            // this.tubeArray[i].note && this.noteGroup.removeChild(this.tubeArray[i].note)
            this.renderNote(this.tubeArray[i]);
        }
    }

    private adsorbNote (note, mx, cb) {
        const player = this.player;
        const playerGroup = this.invinciblePlayerGroup;
        let tw = egret.Tween.get(note);
        tw.to({"x" : player.x + player.bitmap.width - mx + tubeSpeed * 3, "y" : playerGroup.y - player.bitmap.width / 2}, 66);
        tw.call(() => {
            this.noteGroup.removeChild(note);
            cb && cb();
        }, this);
    }

    
    private initPreCountDown (cb:() => void) {
        const preCountDown = new PreCownDown();
        this.stage.addChild(preCountDown);
        preCountDown.addEventListener('FINISH', () => {
            let tw = egret.Tween.get(preCountDown);
            tw.to({"alpha" : 0}, 200);
            tw.call(() => {
                cb && cb();
                bgmusic_canplay = true;
                if(this.musicOn) {
                    this.dispatchEvent(new egret.Event("PLAY_MUSIC", false, false, true));
                }
                this.stage.removeChild(preCountDown);
            }, this);
        }, this);
        preCountDown.addEventListener('COUNTDOWN_END', () => {
            this.createSound('music_go_mp3');
        }, this);
        preCountDown.addEventListener('COUNTDOWN', () => {
            this.createSound('music_number_mp3');
        }, this);
    }

    private failGame () {
        this.onGameOver()
        // return false // todo
        let failBox = new FailBox();
        this.stage.addChild(failBox);
        failBox.addEventListener('REPLAY', () => {
            // let tw = egret.Tween.get(failBox);
            // tw.to({"alpha" : 0}, 200);
            // tw.call(() => {
            //     this.stage.removeChild(failBox);
            //     this.replayGame();
            // }, this);
            
            this.stage.removeChild(failBox);
            this.replayGame();
        }, this);
    }

    private replayGame () {

        this.invincible = 0;
        window['_jooxGame']['onFailPlayAgainTap'] && window['_jooxGame']['onFailPlayAgainTap']();
        this.score = 0;
        scores = new Array(tube_num);
        this.updateScore(0);
        this.gameState = gameState.PENDING;
        this.tubeGroup.x = 0;
        this.noteGroup.x = 0;
        this.resetBg();
        this.cloudGroup.x = this.bgGroup.x;
        this.successDoorGroup.x = this.endX;
        this.successDoorTop.x = this.successDoorTopX;
        this.bonusset = 0;
        this.bonusIndex = 15;
        // this.bonusIndex = getRandForNum(15, 18);
        // this.bonusIndex = getRandForNum(1,2);
        this.finishNum = 0;
        this.player.reset();
        this.resetNotes();
        bgmusic_canplay = false;
        // console.log('replayGame', this.musicOn)
        if (this.musicOn) {
            this.dispatchEvent(new egret.Event("STOP_MUSIC", false, false, true))
        }
        this.initPreCountDown(() => {
            setTimeout(() => {
                this.gameState = gameState.START;
            }, 100)
        })
        this.stage.setChildIndex(this.scoreBar, 9);
    }
    

    private createSound (name:string) {
        if (!this.musicOn) return false;
        let bgm:egret.Sound = RES.getRes( name );
        let channel:egret.SoundChannel = bgm.play(0,1);
        // channel.volume = 1;
        this.soundChannels.push(channel);
        setTimeout(() => {
            if (this.soundChannels.length) {
                this.soundChannels.shift();
            }
        }, 1000)
    }

    private createBitmap (name:string, props?) {
        const bitmap = this.createBitmapByName(name);
        if (props) {
            props.x && (bitmap.x = props.x);
            props.y && (bitmap.y = props.y);
        }
        return bitmap;
    }
    
    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
}