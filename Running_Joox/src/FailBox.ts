/*
 * @Author: Jason
 * @Date: 2019-12-24 09:58:06
 * @LastEditors  : Jason
 * @LastEditTime : 2020-01-10 17:06:25
 */
class FailBox extends egret.DisplayObjectContainer {
    flames: egret.Bitmap[];
    public constructor () {
        super();
        this.once(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);

    }

    private props = {
        headerHeight: 150,
        safeX: 0,
        safeY: 0,
        safeWidth: 0,
        safeHeight: 0,
        ratioX: 0,
        ratio: 0,
        scale: 1,
    }

    public initStageProps () {
        let initStageProps = window['_initStageProps'];
        this.props = initStageProps();
    }

    private onAddToStage () {
        this.initStageProps();
        this.renderBg();
        this.showFailBox();
    }

    private renderBg () {
        const maskBg = new egret.Shape();
        maskBg.x = 0;
        maskBg.y = 0;
        maskBg.width = this.stage.stageWidth
        maskBg.height = this.stage.stageHeight;
        maskBg.alpha = 0;
        maskBg.graphics.beginFill(0x000000);
        maskBg.graphics.drawRect(0, 0, maskBg.width, maskBg.height);
        maskBg.graphics.endFill();
        this.addChild(maskBg);
        const tw = egret.Tween.get(maskBg);
        tw.to({ "alpha": 0.8 }, 200)
    }

    public showFailBox () {
        const props = this.props;
        const ratio = props.ratioX;
        const centerX = this.stage.stageWidth / 2;
        const group = new eui.Group();
        this.addChild(group);
        group.x = centerX;
        group.y = this.stage.stageHeight / 2 - 10 * ratio;
        const failBg = new egret.Shape();
        failBg.width = 660 / 2 * ratio;
        failBg.height = 892 / 2 * ratio;
        failBg.graphics.beginFill(0x8e69ff);
        failBg.graphics.drawRoundRect(0, 0, failBg.width, failBg.height, 40, 40);
        failBg.graphics.endFill();
        failBg.x = 0;
        failBg.y = -10 * ratio;
        failBg.anchorOffsetX = failBg.width / 2;
        failBg.anchorOffsetY = failBg.height / 2;
        group.alpha = 0;
        group.scaleX = 0;
        group.scaleY = 0;
        group.addChild(failBg);

        const failMain= this.createBitmapByName('lose_main_png');
        // failBg.width = 554 / 2 * ratio;
        // failBg.height = 541 / 2 * ratio;
        failMain.x = 0;
        failMain.y = -35 * ratio;
        failMain.anchorOffsetX = failMain.width / 2;
        failMain.anchorOffsetY = failMain.height / 2;

        group.addChild(failMain);

        // const failTitle= this.createBitmapByName('lose_T1_png');
        // failTitle.width = 574 / 2 * ratio;
        // failTitle.height = 102 / 2 * ratio;
        // failTitle.x = 0;
        // failTitle.y = -failMain.height / 2 - failTitle.height / 2 - 55 * ratio;
        // failTitle.anchorOffsetX = failTitle.width / 2;
        // failTitle.anchorOffsetY = failTitle.height / 2;

        // group.addChild(failTitle);
        
        const failTitle:egret.TextField = new egret.TextField();
        failTitle.textColor = 0xffffff;
        failTitle.stroke = 3;
        failTitle.strokeColor = 0x000000;
        failTitle.size = (window['myTexts']['failTitleFS'] || 92) * props.scale;
        // failTitle.bold = true;
        failTitle.fontFamily = window['myTexts']['failTitleFF'] || 'JOOX Brand Regular';
        failTitle.text = window['myTexts']['failTitle'];
        failTitle.x = (window['myTexts']['failTitleX'] || 0) * props.scale;
        failTitle.y = -failMain.height / 2 - failTitle.height / 2 - 55 * ratio;
        failTitle.anchorOffsetX = failTitle.width / 2;
        failTitle.anchorOffsetY = failTitle.height / 2;
        group.addChild(failTitle);
        
        const desc:egret.TextField = new egret.TextField();
        const descMark:egret.TextField = new egret.TextField();
        desc.textColor = 0x000000;
        desc.size = (window['myTexts']['failTextFS'] || 36) * props.scale;
        desc.fontFamily = window['myTexts']['failTextFF'] || 'Joox Brand Regular';
        descMark.textColor = 0x000000;
        descMark.size = 28 * props.scale;
        descMark.bold = true;
        descMark.fontFamily = 'PingFang SC, helvetica neue, Microsoft YaHei';
        desc.text = window['myTexts']['failText'];
        descMark.text = '？';
        // desc.x = -5;
        desc.x = (window['myTexts']['failTextX'] || -5) * props.scale;
        desc.y = failMain.height / 2 + 1;
        desc.anchorOffsetX = desc.width / 2;
        desc.anchorOffsetY = desc.height / 2;
        descMark.x = desc.x + desc.anchorOffsetX;
        descMark.y = desc.y;
        descMark.anchorOffsetY = desc.anchorOffsetY;
        group.addChild(desc);
        if (window['_language'] == 'en') {
            group.addChild(descMark);
        }
        
        const playAgain = this.createShapeButton({
            x: 0,
            y: desc.y + desc.height + 40 * ratio,
            width: 545 / 2 * ratio,
            height: 92 / 2 * ratio,
            bgColor: 0xfff72a,
            color: 0x000000,
            size: (window['myTexts']['failButtonFS'] || 36) * props.scale,
            text:  window['myTexts']['failButton']
        });
        group.addChild(playAgain.group);
        playAgain.button.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let tw = egret.Tween.get(playAgain.group);
            tw.to({"scaleX": 1, "scaleY": 1}, 100);
            tw.call(() => {
                group.removeChild(playAgain.group);
                let tw2 = egret.Tween.get(group);
                tw2.to({"alpha" : 0}, 200);
                tw2.call(() => {
                    this.dispatchEvent(new egret.Event("REPLAY", false, false, true));
                }, this);
            }, this);
        }, this);
        playAgain.button.addEventListener(egret.TouchEvent.TOUCH_BEGIN, () => {
            let tw = egret.Tween.get(playAgain.group);
            tw.to({"scaleX": 0.9, "scaleY": 0.9}, 100);
        }, this);

        let tw = egret.Tween.get(group);
        tw.to({ "alpha": 1, "scaleX": 1, "scaleY": 1 }, 500, egret.Ease.cubicInOut);
        
    }
    private createShapeButton (props) {
        const group = new eui.Group();
        group.x = props.x;
        group.y = props.y;
        const shapeWidth = props.width;
        const shapeHeight = props.height;
        const radius = shapeHeight / 2;
        const shape = new egret.Shape();
        shape.width = shapeWidth - radius * 2;
        shape.height = shapeHeight;
        const shapeShadow = new egret.Shape();
        shapeShadow.width = shape.width;
        shapeShadow.height = shapeHeight + 8;
        const radius2 = shapeShadow.height / 2;
        shapeShadow.graphics.beginFill(0x000000);
        // shapeShadow.graphics.lineStyle(4, 0x000000);
        // shapeShadow.graphics.drawRoundRect(0, 0, shapeWidth, shapeHeight, radius2, radius2);
        shapeShadow.graphics.drawRect(0, 0, shapeShadow.width, shapeShadow.height);
        shapeShadow.graphics.endFill();
        group.addChild(shapeShadow);
        shapeShadow.anchorOffsetX = shapeShadow.width / 2;
        shapeShadow.anchorOffsetY = shapeShadow.height / 2;
        shapeShadow.x = 0;
        shapeShadow.y = 2;

        
        const circleLeftShadow = new egret.Shape();
        circleLeftShadow.width = radius2 * 2;
        circleLeftShadow.height = radius2 * 2;
        circleLeftShadow.x = -shapeShadow.anchorOffsetX;
        circleLeftShadow.y = 2;
        circleLeftShadow.graphics.beginFill(0x000000);
        // circleLeftShadow.graphics.lineStyle(4, 0x000000);
        circleLeftShadow.graphics.drawCircle(0, 0, radius2);
        circleLeftShadow.graphics.endFill();
        group.addChild(circleLeftShadow);
        
        const circleRightShadow = new egret.Shape();
        circleRightShadow.width = radius2 * 2;
        circleRightShadow.height = radius2 * 2;
        circleRightShadow.x = shapeShadow.anchorOffsetX;
        circleRightShadow.y = 2;
        circleRightShadow.graphics.beginFill(0x000000);
        // circleRightShadow.graphics.lineStyle(4, 0x000000);
        circleRightShadow.graphics.drawCircle(0, 0, radius2);
        circleRightShadow.graphics.endFill();
        group.addChild(circleRightShadow);



        shape.graphics.beginFill(props.bgColor || 0xffffff);
        // shape.graphics.lineStyle(4, 0x000000);
        // shape.graphics.drawRoundRect(0, 0, shapeWidth, shapeHeight, shapeHeight, shapeHeight);
        shape.graphics.drawRect(0, 0, shape.width, shapeHeight);
        shape.graphics.endFill();
        group.addChild(shape);
        shape.anchorOffsetX = shape.width / 2;
        shape.anchorOffsetY = shape.height / 2;
        shape.x = 0;
        shape.y = 0;
        
        const circleLeft = new egret.Shape();
        circleLeft.width = radius * 2;
        circleLeft.height = radius * 2;
        circleLeft.x = -shape.anchorOffsetX;
        circleLeft.y = 0;
        circleLeft.graphics.beginFill(props.bgColor || 0xffffff);
        circleLeft.graphics.drawCircle(0, 0, radius);
        circleLeft.graphics.endFill();
        group.addChild(circleLeft);
        
        const circleRight = new egret.Shape();
        circleRight.width = radius * 2;
        circleRight.height = radius * 2;
        circleRight.x = shape.anchorOffsetX;
        circleRight.y = 0;
        circleRight.graphics.beginFill(props.bgColor || 0xffffff);
        circleRight.graphics.drawCircle(0, 0, radius);
        circleRight.graphics.endFill();
        group.addChild(circleRight);

        let button = new eui.Button();
        button.width = shapeWidth;
        button.height = shapeHeight;
        button.x = 0;
        button.y = 0;
        button.alpha = 0;
        button.anchorOffsetX = shapeWidth / 2;
        button.anchorOffsetY = shapeHeight / 2;
        group.addChild(button);
        
        const btnText:egret.TextField = new egret.TextField();
        btnText.textColor = props.color || 0x000000;
        btnText.size = props.size || 36;
        btnText.fontFamily = window['myTexts']['failButtonFF'] || 'PingFang SC, helvetica neue, Microsoft YaHei';
        btnText.text = props.text;
        btnText.bold = typeof window['myTexts']['failButtonBold'] == 'undefined' ? true : window['myTexts']['failButtonBold'];
        btnText.x = shape.x;
        btnText.y = shape.y;
        btnText.anchorOffsetX = btnText.width / 2;
        btnText.anchorOffsetY = btnText.height / 2;
        group.addChild(btnText);
        return {
            group: group,
            shape: shape,
            button: button,
            btnText: btnText
        };
    }

    private createBitmap (name:string, props?) {
        const bitmap = this.createBitmapByName(name);
        if (props) {
            props.x && (bitmap.x = props.x);
            props.y && (bitmap.y = props.y);
            props.width && (bitmap.width = props.width);
            props.height && (bitmap.height = props.height);
        }
        return bitmap;
    }
    private createBitmapButton (name: string, props?) {
        let bitmap = this.createBitmap(name, props);
        this.addChild(bitmap);
        bitmap.anchorOffsetX = bitmap.width / 2;
        bitmap.anchorOffsetY = bitmap.height / 2;

        let button = new eui.Button();
        button.width = bitmap.width;
        button.height = bitmap.height;
        button.x = bitmap.x;
        button.y = bitmap.y;
        button.alpha = 0;
        button.anchorOffsetX = bitmap.anchorOffsetX;
        button.anchorOffsetY = bitmap.anchorOffsetY;
        this.addChild(button);

        return {
            bitmap: bitmap,
            button: button
        };
    }

    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
}