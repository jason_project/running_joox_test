//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class Main extends eui.UILayer {


    protected createChildren(): void {
        super.createChildren();

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }

        //inject the custom material parser
        //注入自定义的素材解析器
        let assetAdapter = new AssetAdapter();
        egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());


        this.runGame().catch(e => {
            console.log(e);
        })
    }

    
    private musicOn: boolean = false;
    private musicIcon: egret.Bitmap;
    
    private loadingView: LoadingUI;
    private loadingFinish: boolean = false;

    private async runGame() {
        this.props = this.initStageProps();
        window['_initStageProps'] = this.initStageProps.bind(this);
        await this.loadResource()
        // this.createGameScene();
        // const result = await RES.getResAsync("description_json")
        // this.startAnimation(result);
        // await platform.login();
        // const userInfo = await platform.getUserInfo();
        // console.log(userInfo);

    }

    private async loadResource() {
        try {
            const loadingView = new LoadingUI();
            this.stage.addChild(loadingView);
            
            // loadingView.addEventListener('ICON_FINISH', async () => {
                this.preUseFont();
                await RES.loadConfig((window['_basePath'] || '') + "resource/" + window['_language'] + "/default.res.json", (window['_basePath'] || '') + "resource/");
                await this.loadTheme();
                await RES.loadGroup("preload", 0, loadingView);
                this.renderMusicIcon();
            // }, this);
            loadingView.addEventListener('START', () => {
                window['_jooxGame']['onGameStartTap'] && window['_jooxGame']['onGameStartTap']();
                this.loadingFinish = true;
                this.loadingView = loadingView;
                let tw = egret.Tween.get(loadingView);
                tw.to({"alpha" : 0}, 200);
                tw.call(() => {
                    this.stage.removeChild(loadingView);
                    this.createGameScene();
                }, this);
            }, this);
        }
        catch (e) {
            console.error(e);
        }
    }

    private loadTheme() {
        return new Promise((resolve, reject) => {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, () => {
                resolve(1);
            }, this);

        })
    }
    private bgmChannels: egret.SoundChannel [] = [];

    public props = {
        headerBarHeight: 106,
        headerHeight: 150,
        safeX: 0,
        safeY: 0,
        safeWidth: 0,
        safeHeight: 0,
        ratioX: 2,
        ratio: 2,
        scale: 1,
        shape: 'normal',
    }

    public initStageProps () {
        let { stageWidth: stageW, stageHeight: stageH } = this.stage;
        let { innerWidth: windowWidth, innerHeight: windowHeight } = window;

        const deviceRatio = windowWidth / windowHeight;
        const standardRatio = 750 / 1334;

        let shape = 'normal';
        if (deviceRatio > standardRatio) {
            shape = 'fat';
        }
        if (deviceRatio < standardRatio) {
            shape = 'thin';
        }

        let ratio = 0;
        let widthRatio = stageW / windowWidth
        let heightRatio = stageH / windowHeight

        let safeX = 0;
        let safeY = 0;
        let safeWidth = stageW;
        let safeHeight = stageH;
        if (this.stage.scaleMode === egret.StageScaleMode.NO_BORDER) {
            if (widthRatio > heightRatio) {
                ratio = heightRatio
                safeWidth = windowWidth * ratio
                safeX = (stageW - safeWidth) / 2;
            } else {
                ratio = widthRatio
                safeHeight = windowHeight * ratio
                safeY = (stageH - safeHeight) / 2;
            }
        } else if (this.stage.scaleMode === egret.StageScaleMode.SHOW_ALL) {
            if (widthRatio > heightRatio) {
                ratio = heightRatio
                safeHeight = windowHeight * ratio
                safeY = 0;
            } else {
                ratio = widthRatio
                safeWidth = windowWidth * ratio
                safeX = 0;
            }
        }

        let ratioX = safeWidth / 375;
        let scale = windowWidth / 375;
        // let headerBarHeight = this.props.headerBarHeight * scale;
        // let headerHeight = this.props.headerHeight * scale;
        const props = {
            ...this.props,
            // headerBarHeight,
            // headerHeight,
            safeX,
            safeY,
            safeWidth,
            safeHeight,
            ratioX,
            ratio,
            scale,
            shape
        }
        return props;
    }
    
    /**
     * 创建场景界面
     * Create scene interface
     */
    protected createGameScene(): void {
        window['_jooxGame']['onGameStart'] && window['_jooxGame']['onGameStart']();
                
        let { stageWidth: stageW, stageHeight: stageH } = this.stage;
        // console.log(stageW, stageH)
        
        let playGround:PlayGround = new PlayGround({
            duration: 15,
            musicOn: this.musicOn
        });
        playGround.addEventListener('TOGGLE_MUSIC', () => {
            this.toggleMusicIcon(this.musicIcon);
        }, this)
        playGround.addEventListener('PLAY_MUSIC', () => {
            this.playMusicIcon();
        }, this)
        playGround.addEventListener('STOP_MUSIC', () => {
            this.stopMusicIcon();
        }, this)
        this.addChild(playGround);
    }

    
    private renderMusicIcon () {
        const props = this.props
        const { ratioX: ratio, scale  } = props
        let name = this.musicOn ? 'sound_on_png' : 'sound_off_png';
        const obj = this.createBitmapButton(name, {
            x: props.safeX + props.safeWidth - 26 * ratio,
            y: props.safeY + 24 * ratio
        })
        const musicIcon = obj.bitmap;
        this.musicIcon = musicIcon;
        
        if (this.musicOn) {
            this.rotateMusicIcon(obj.bitmap);
            const bgmChannels = this.bgmChannels;
            if (!bgmChannels.length) {
                this.playBgmChannelMusic('music_bg_mp3', 0)
            }
        }
        
        window['joox_toggleMusicIcon'] = () => {
            this.toggleMusicIcon(this.musicIcon);
        };
        console.log(window['sessionStorage']['musicOn'])
        if (window['sessionStorage']['musicOn'] == '1') {
            this.toggleMusicIcon(this.musicIcon);
        }
        obj.button.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.toggleMusicIcon(obj.bitmap);
        }, this)
        const hiddenProperty = 'hidden' in document ? 'hidden' :
                            'webkitHidden' in document ? 'webkitHidden' :
                            'mozHidden' in document ? 'mozHidden' :
                            null;
        const visibilityChangeEvent = hiddenProperty.replace(/hidden/i, 'visibilitychange');
        const self = this;
        var onVisibilityChange = function() {
             if (self.musicOn) {
                 const bgmChannels = self.bgmChannels;
                 if (!document[hiddenProperty]) {
                     if (!bgmChannels.length) {
                         self.playBgmChannelMusic('music_bg_mp3', 0)
                     }
                 } else {
                     for (let m of bgmChannels) {
                         try {
                             m.stop();
                             bgmChannels.pop();
                         } catch (e) {
                             console.log(e)
                         }
                     }
                 }
             }
        }
        document.addEventListener(visibilityChangeEvent, onVisibilityChange);
    }

    
    private rotateMusicIcon (result: egret.Bitmap) {
        let go = () => {
            let tw = egret.Tween.get(result);
            tw.to({ "rotation": 360 }, 1500);
            result.rotation = 0;
            tw.call(go, this);
        }
        go();
    }

    private toggleMusicIcon (result: egret.Bitmap) {
        this.musicOn = !this.musicOn;
        const musicOn = this.musicOn;
        window['sessionStorage']['musicOn'] = musicOn ? '1' : '0';
        let name = musicOn ? 'sound_on_png' : 'sound_off_png';
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        const bgmChannels = this.bgmChannels;
        console.log(musicOn, name)
        if (musicOn) {
            this.rotateMusicIcon(result);
            if (!bgmChannels.length) {
                this.playBgmChannelMusic('music_bg_mp3', 0)
            }
        } else {
            egret.Tween.removeTweens(result);
            result.rotation = 0;
            for (let m of bgmChannels) {
                try {
                    m.stop();
                    bgmChannels.pop();
                } catch (e) {
                    console.log(e)
                }
            }
        }
    }

    private playMusicIcon () {
        const bgmChannels = this.bgmChannels;
        console.log('playMusicIcon', bgmChannels.length)
        bgmChannels.pop();
        if (!bgmChannels.length) {
            this.playBgmChannelMusic('music_bg_mp3', 0)
        }
    }

    private stopMusicIcon () {
        const bgmChannels = this.bgmChannels;
        console.log('stopMusicIcon', bgmChannels.length)
        for (let m of bgmChannels) {
            try {
                m.stop();
                bgmChannels.pop();
            } catch (e) {
                console.log(e)
            }
        }
    }

    private playBgmChannelMusic (name: string, volume: number) {
        let bgm:egret.Sound = RES.getRes(name);
        let channel:egret.SoundChannel = bgm.play(0, volume);
        // channel.volume = 1;
        this.bgmChannels.push(channel);
        window["_jooxGame"]["playsound"](bgm, false);
    }
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
    private createBitmapButton (name: string, props) {
        let bitmap = this.createBitmapByName(name);
        this.addChild(bitmap);
        bitmap.anchorOffsetX = bitmap.width / 2;
        bitmap.anchorOffsetY = bitmap.height / 2;
        bitmap.x = props.x;
        bitmap.y = props.y;

        let button = new eui.Button();
        button.width = bitmap.width + 20;
        button.height = bitmap.height + 20;
        button.x = bitmap.x - 10;
        button.y = bitmap.y - 10;
        button.alpha = 0;
        button.anchorOffsetX = bitmap.anchorOffsetX;
        button.anchorOffsetY = bitmap.anchorOffsetY;
        this.addChild(button);

        return {
            bitmap: bitmap,
            button: button
        };
    }

    private preUseFont () {
        const preRegular:egret.TextField = new egret.TextField();
        const preBold:egret.TextField = new egret.TextField();
        preRegular.textColor = 0xffffff;
        preBold.textColor = 0xffffff;
        preRegular.size = 1;
        preBold.size = 1;
        preRegular.x = -100;
        preBold.x = -100;
        preRegular.fontFamily = 'Joox Brand Regular';
        preBold.fontFamily = 'Joox Brand Bold';
        preRegular.text = 'a';
        preBold.text = 'a';
        preRegular.alpha = 1;
        preBold.alpha = 1;
        this.addChild(preRegular); 
        this.addChild(preBold);
        if (window['myTexts']['useSelfFont']) {
            const preSelfFont:egret.TextField = new egret.TextField();
            preSelfFont.textColor = 0xffffff;
            preSelfFont.size = 1;
            preSelfFont.x = -100;
            preSelfFont.fontFamily = window['myTexts']['useSelfFont'];
            preSelfFont.text = 'a';
            preSelfFont.alpha = 1;
            this.addChild(preSelfFont);
        }
    }

    /**
     * 点击按钮
     * Click the button
     */
    private onButtonClick(e: egret.TouchEvent) {
        let panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    }
}
